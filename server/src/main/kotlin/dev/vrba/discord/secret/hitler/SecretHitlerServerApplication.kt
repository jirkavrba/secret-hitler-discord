package dev.vrba.discord.secret.hitler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SecretHitlerServerApplication

fun main(args: Array<String>) {
    runApplication<SecretHitlerServerApplication>(*args)
}
