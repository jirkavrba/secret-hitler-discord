package dev.vrba.discord.secret.hitler.engine

enum class Ability {
    ExamineTopThreeCards,
    InvestigatePartyMembership,
    SelectNextPresidentialCandidate,
    ExecutePlayer,
}
